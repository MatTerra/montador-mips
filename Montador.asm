## Montador de Assembly desenvolvido em Assembly MIPS
## @author Júlio Eduardo França Dumont
## @author Mateus Berardo de Souza Terra
## @contributor Stephanie C. Guimarães
## @date 05/05/2019


.data
# Monta uma tabela de instruções e máscaras com func e opcode definidos. O endereço base será da instrução
# Instrução+4 será a máscara

dbg:	.word 0
dbg2:	.word 0
_add: 	.ascii "add"
_madd: 	.word 0x00000020 
_tadd:	.word 0
_lw: 	.ascii "lw"
_mlw: 	.word 0x8c000000
_tlw:	.word 1
_sub: 	.ascii "sub"
_msub: 	.word 0x00000022 
_tsub:	.word 0
_and: 	.ascii "and"
_mand: 	.word 0x00000024
_tand:	.word 0
_or: 	.ascii "or"
_mor: 	.word 0x00000025
_tor:	.word 0
_nor: 	.ascii "nor"
_mnor: 	.word 0x00000027
_tnor:	.word 0
_xor: 	.ascii "xor"
_mxor: 	.word 0x00000026
_txor:	.word 0
_sw: 	.ascii "sw"
_msw: 	.word 0xac000000
_tsw:	.word 1
_j: 	.ascii "j"
_mj: 	.word 0x08000000
_tj:	.word 2
_jr: 	.ascii "jr"
_mjr: 	.word 0x00000008
_tjr:	.word 3
_jal: 	.ascii "jal"
_mjal:	.word 0x0c000000
_tjal:	.word 2
_beq: 	.ascii "beq"
_mbeq: 	.word 0x10000000
_tbeq:	.word 4
_bne: 	.ascii "bne"
_mbne: 	.word 0x14000000
_tbne:	.word 4
_slt: 	.ascii "slt"
_mslt: 	.word 0x0000002a
_tslt:	.word 0
_lui: 	.ascii "lui"
_mlui: 	.word 0x3c000000
_tlui:	.word 5
_addu: 	.ascii "addu"
_maddu: .word 0x00000021
_taddu:	.word 0
_subu: 	.ascii "subu"
_msubu: .word 0x00000023
_tsubu:	.word 0
_sll: 	.ascii "sll"
_msll: 	.word 0x00000000
_tsll:	.word 6
_srl: 	.ascii "srl"
_msrl: 	.word 0x00000002
_tsrl:	.word 6
_addi: 	.ascii "addi"
_maddi: .word 0x20000000
_taddi:	.word 7
_andi: 	.ascii "andi"
_mandi: .word 0x30000000
_tandi:	.word 7
_ori: 	.ascii "ori"
_mori: 	.word 0x34000000 
_tori:	.word 7
_xori: 	.ascii "xori"
_mxori: .word 0x38000000
_txori:	.word 7
_mult: 	.ascii "mult"
_mmult: .word 0x00000018
_tmult:	.word 8
_div: 	.ascii "div"
_mdiv: 	.word 0x0000001a
_tdiv:	.word 8
_li: 	.ascii "li"
_mli: 	.word 0x00000000 # Não existe
_tmli:	.word -1
_mfhi: 	.ascii "mfhi"
_mmfhi: .word 0x00000010
_tmfhi:	.word 9
_mflo: 	.ascii "mflo"
_mmflo: .word 0x00000012
_tmflo:	.word 9
_bgez: 	.ascii "bgez"
_mbgez: .word 0x04010000
_tbgez:	.word 10
_clo: 	.ascii "clo"
_mclo: 	.word 0x70000021
_tclo:	.word 11
_srav: 	.ascii "srav"
_msrav: .word 0x00000007
_tsrav: .word 0
zeroreg: .asciiz "$zero"
.align 2
at:		.asciiz	"$at"
at_t:		.asciiz "$1\0"
v0:		.asciiz "$v0"
v0_t:		.asciiz "$2\0"
v1:		.asciiz "$v1"
v1_t:		.asciiz "$3\0"
a0:		.asciiz "$a0"
a0_t:		.asciiz "$4\0"
a1:		.asciiz "$a1"
a1_t:		.asciiz "$5\0"
a2:		.asciiz "$a2"
a2_t:		.asciiz "$6\0"
a3:		.asciiz "$a3"
a3_t:		.asciiz "$7\0"
t0:		.asciiz "$t0"
t0_t:		.asciiz "$8\0"
t1:		.asciiz "$t1"
t1_t:		.asciiz "$9\0"
t2:		.asciiz "$t2"
t2_t:		.asciiz "$10"
t3:		.asciiz "$t3"
t3_t:		.asciiz "$11"
t4:		.asciiz "$t4"
t4_t:		.asciiz "$12"
t5:		.asciiz "$t5"
t5_t:		.asciiz "$13"
t6:		.asciiz "$t6"
t6_t:		.asciiz "$14"
t7:		.asciiz "$t7"
t7_t:		.asciiz "$15"
s0:		.asciiz "$s0"
s0_t:		.asciiz "$16"
s1:		.asciiz "$s1"
s1_t:		.asciiz "$17"
s2:		.asciiz "$s2"
s2_t:		.asciiz "$18"
s3:		.asciiz "$s3"
s3_t:		.asciiz "$19"
s4:		.asciiz "$s4"
s4_t:		.asciiz "$20"
s5:		.asciiz "$s5"
s5_t:		.asciiz "$21"
s6:		.asciiz "$s6"
s6_t:		.asciiz "$22"
s7:		.asciiz "$s7"
s7_t:		.asciiz "$23"
t8:		.asciiz "$t8"
t8_t:		.asciiz "$24"
t9:		.asciiz "$t9"
t9_t:		.asciiz "$25"
k0:		.asciiz "$k0"
k0_t:		.asciiz "$26"
k1:		.asciiz "$k1"
k1_t:		.asciiz "$27"
gpreg:		.asciiz "$gp"
gpreg_t:	.asciiz	"$28"
spreg:		.asciiz "$sp"
spreg_t:	.asciiz "$29"
fpreg:		.asciiz "$fp"
fpreg_t:	.asciiz "$30"
rareg:		.asciiz "$ra"
rareg_t:	.asciiz "$31"
.align 2
file: 		.asciiz "arquivo.asm"
datamif:	.asciiz "data.mif"
compactfile:	.asciiz "temp.txt"
labelfile:      .asciiz "labels.txt"
.align 2
data_label: 	.ascii "data"
text_label: 	.ascii "text"
dotword:	.ascii "word"
dois_pontos: 	.ascii ":"
dolar: 		.ascii "$"
new_line: 	.ascii "\n"
space: 		.ascii " "
ponto: 		.ascii "."
virg: 		.ascii ","
tab: 		.ascii "\t"
ibuffer:	.byte 0 
mifspace:	.asciiz " : "
semicolon: 	.ascii ";\n"
.align 2
end:		.ascii "\nEND;"
.align 2 
header:		.ascii "DEPTH = 16384;\nWIDTH = 32;\nADRESS_RADIX = HEX;\nDATA_RADIX = HEX;\nCONTENT\nBEGIN\n\n"
headerlen:	.word 80
.align 2
zerox:		.asciiz "0x"
writingbuffer:  .word 0 0
counter:	.word 0x10010000
datalblcounter: .word 0x10010000
buffer: 	.space 20
buffer2:	.word 0 0 0 0 0 0 0 0 0 0 0
escrita:	.space 4
labelbuffer: 	.word 0 0 0 0 0 
labelbuffer2: 	.word 0 0 0 0 
.align 2
databuffer:	.space 512
.align 2
bufferi:	.space 10
.align 2
headertxt: .ascii "DEPTH = 4096;\nWIDTH = 32;\nADRESS_RADIX = HEX;\nDATA_RADIX = HEX;\nCONTENT\nBEGIN\n\n"
headertxtlen:	.word 79
instructionpair: .word 0 0
textmif:	.asciiz "text.mif"
read_buffer:	.word 0 0
ld_buffer:	.word 0
add_buffer:	.word 0 0


.text

	j 	wrtdatafile		# Processa o .data

Abrir2:	
	li 	$v0, 13	
	la 	$a0, compactfile     	# Carrega o nome do arquivo
	li	$a1, 0       	 	# flag da leitura (0 leitura, 1 escrita)
	li 	$a2, 0        		# mode é ignorado
	syscall 
	move 	$s0, $v0 	 	# Salva o código do arquivo
	jr	$ra			# Retorna para caller	


# Abrir arquivo para leitura
Abrir:	
	li 	$v0, 13	
	la 	$a0, file     	  	# Carrega o nome do arquivo
	li	$a1, 0       	 	# flag da leitura (0 leitura, 1 escrita)
	li 	$a2, 0        		# mode é ignorado
	syscall 
	move 	$s0, $v0 	 	# Salva o código do arquivo
	
# Ler 20 bytes do Arquivo
Ler: 	
	addi	$sp, $sp, -4		# Abre um espaço na pilha
	sw	$v0, 0($sp)		# Empilha v0
	li   	$v0, 14      		# Carrega o código de leitura de arquivo
	move 	$a0, $s0      		# Código do arquivo 
	la   	$a1, buffer   		# endereço do buffer
	li  	$a2,  20      		# tamanho do buffer
	syscall      			# Lê do arquivo
	move	$s3, $v0		# salva a quantidade de bytes no buffer
	blt 	$s3, $zero, exit	# Caso tenha um erro (s3<0), termina o programa
	addi	$s3, $s3, -1		# Transforma em indice máximo
	li 	$s1, -1			# Carrega endereço do buffer
	lw	$v0, 0($sp)		# Desempilha v0
	addi	$sp, $sp, 4		# Retorna a pilha para posição
	beq	$s4, 1, continue_ld_nxt	# Continua carregando 

# Busca um ponto no código e a partir dele, busca o .text	
lct_pt:	
	la 	$t0, ponto		# Carrega o endereço do literal .
	lbu 	$t0, ($t0)		# Carrega o literal .
	la 	$ra, cmp_pt		# Carrega o endereço da função no registrador a3 
	j 	ld_nxt			# Carrega o próximo caractere	
cmp_pt:	
	beq 	$s2, $t0, is_txt	# Verifica se o byte lido é um .
	la 	$ra, cmp_pt		# Carrega o endereço da função no registrador a3 
	j 	ld_nxt			# Carrega o próximo caractere		

ld_nxt:	
	addi 	$sp, $sp, -4		# abre um espaço na pilha
	sw 	$t3, 0($sp)		# Armazena t3 na pilha
continue_ld_nxt:
	beq	$s3, -1, fim_ld_nxt	# Não há bytes lidos, finaliza
	sub 	$t3, $s1, $s3		# Subtrai s3 do contador para verificar se percorreu todos os bytes lidos
	beq	$zero, $t3, Ler		# Se tiver lido os s3, lê mais 20(max)	
	addi 	$s1, $s1, 1		# Incrementa s1 para percorrer o buffer
	lbu 	$s2, buffer($s1)	# Carrega um byte do buffer
	sw	$s2, dbg2($zero)	# Carrega a letra lida para debug
fim_ld_nxt:
	lw 	$t3, 0($sp)		# Remove t3 da pilha
	addi 	$sp, $sp, 4		# Corrige ponteiro da pilha
	jr 	$ra			# Retorna para caller
	
is_txt: 
	jal 	ldw			# Pula para carregar os próximos caracteres como word
	move	$s2, $v0		# Carrega o resultado de ldw em $s2
	la 	$t0, text_label		# Carrega o endereço de "text"
	lw 	$t0, ($t0)		# Carrega "text"
	beq 	$s2, $t0, p_prog	# Compara text com a label encontrada
	la 	$ra, lct_pt		# Carrega o endereço da função no registrador a3
	j 	ld_nxt			# Continua para o próximo caractere
	
#Carrega até 4 letras/uma word. Essa função serve pra lidar com palavras que tem parte em uma word e parte em outra
ldw:	
	addi 	$sp, $sp, -16		# Abre espaço na pilha
	sw	$t6, 12($sp)		# Empilha t6
	sw 	$ra, 8($sp)		# Empilhar $ra
	sw 	$t8, 4($sp)		# Armazena t8 na pilha
	sw 	$t9, 0($sp)		# Armazena t9 na pilha
	add	$v0, $zero, $zero	# Zera v0
	add	$v1, $zero, $zero	# Zera v1
	add 	$t9, $zero, $zero	# Zera t9
	jal 	ld_nxt			# Carrega o primeiro byte	
ldw_f:	
	move 	$v0, $s2		# Salva o primeiro byte na saida
ldw_s:	
	addi 	$t9, $t9, 8		#Incrementa $t9 que fará o shamt
	jal 	ld_nxt			# Carrega o endereço da continuação
carr_b:	
	beq	$s3, -1, ret_prem	# Não há caracteres mais para ler
	move	$a0, $s2		# Bota o caractere lido em a0
	jal 	is_ws			# Chama a função para verificar se é um caractere em branco
	beq	$a0, 1, ret_prem	# Finaliza se for um ws
	beq	$a0, 2, ret_nw		# Finaliza se for um nw
	move	$a0, $s2		# Bota o caractere lido novamente em a0
	jal 	is_2p			# Verifica se é 2pontos
	beq	$a0, 1, ret_2p		# Finaliza com flag
	sllv 	$t8, $s2, $t9	  	# Desloca ele $t9 bytes para a esquerda
	or 	$v0, $v0, $t8		# Soma ele ao resultado
	lbu 	$t6, virg		# Carrega o literal ,
	beq 	$t6, $t8, ret_comp	# Verifica se o caractere lido é ,
	bne 	$t9, 24, ldw_s		# Caso não tenha carregado 4 bytes, carrega o próximo
	jal 	ld_nxt			# Carrega o byte seguinte
	addi	$s1, $s1, -1		# volta o endereço da leitura do arquivo em 1 unidade
	move	$a0, $s2		# Carrega ele em a0
	jal 	is_ws			# Verifica se é ws
	beq	$a0, 1, ret_prem	# Se for ws, adiciona uma flag de que a palavra está completa
	beq	$a0, 2, ret_nw		# Se for nw, adiciona a flag correspondente
	move	$a0, $s2		# Carrega o byte novamente em a0
	jal	is_2p			# Verifica se o próximo é dois pontos
	beq 	$a0, 1, ret_2p		# retorna se é dois pontos
	
ret_ldw:
	lw	$t6, 12($sp)		# Desempilha t6
	lw 	$ra, 8($sp)		# Desempilhar $ra
	lw 	$t8, 4($sp)		# Desempilhar t8
	lw 	$t9, 0($sp)		# Desempilhar t9
	addi 	$sp, $sp, 16		# Corrigir endereço da pilha
	sw	$v0, dbg		# Salva o valor em dbg para verificação
	jr 	$ra			# Retorna ao ponto onde foi chamado
ret_prem:
	li	$v1, 1			# Carrega uma flag em $v1 que indica q leu menos q 4 bytes/terminou em ws
	j	ret_ldw			# Finaliza a função
ret_comp:
	li	$v1, 4			# Carrega uma flag em $v1 que indica q terminou em virg
	j	ret_ldw			# Finaliza a função
ret_2p:
	li	$v1, 2			# Carrega a flag de 2p em $v1
	j	ret_ldw			# Finaliza a função
ret_nw:
	li	$v1, 3			# Indica que a leitura terminou com \n
	beq	$v0, $zero, ret_ldw	# Pula o retorno do ponteiro caso não tenha lido nenhum caractere diferente de whitespace
	j	ret_ldw			# Finaliza a função


# Verifica se o caractere em $a0 é um white space " ", "\n", "\t"		
is_ws:
	addi 	$sp, $sp, -4		# Abre espaço na pilha
	sw	$t6, 0($sp)		# Guarda $t6 na pilha
	lbu 	$t6, space		# Carrega o literal espaço
	beq 	$t6, $a0, sis_ws	# Verifica se o caractere lido é espaço
	lbu 	$t6, new_line	 	# Carrega o literal \n
	beq 	$t6, $a0, sis_nw	# Verifica se o caractere lido é \n
	lbu 	$t6, tab		# Carrega o literal \t
	beq 	$t6, $a0, sis_ws	# Verifica se o caractere lido é \t
	li	$a0, 0			# Define a resposta como não white space
	j 	fim_is_ws		# finaliza a função
sis_nw:
	li	$a0, 2			# Define a resposta como new line
	j 	fim_is_ws		# Finaliza a função
sis_ws:
	li	$a0, 1			# Define a resposta como white space
fim_is_ws:
	lw	$t6, 0($sp)		# Pega $t6 da pilha
	addi	$sp, $sp, 4		# Volta a pilha para a posição
	jr 	$ra			# retorna pra caller
	
# Verifica se o caractere em a0 é um ":"
is_2p:
	addi 	$sp, $sp, -4		# Abre espaço na pilha
	sw	$t6, 0($sp)		# Guarda $t6 na pilha
	lbu 	$t6, dois_pontos	# Carrega o literal dois pontos
	beq 	$t6, $a0, sis_2p	# Verifica se o caractere lido é dois pontos
	li	$a0, 0			# Define a resposta como não dois pontos
	j 	fim_is_2p		# finaliza a função
sis_2p:
	li	$a0, 1			# Define a resposta como dois pontos
fim_is_2p:
	lw	$t6, 0($sp)		# Pega $t6 da pilha
	addi	$sp, $sp, 4		# Volta a pilha para a posição
	jr 	$ra			# retorna pra caller
	
pular_coment:
	jal 	ld_nxt			# carrega o proximo byte
	beq	$s2, '\n', p_prog_loop	# continua o processamento
	j 	pular_coment		# continua lendo pra descartar comentarios	

wrtbfr2file:
	add	$t0, $zero, $zero	# zera $t0
	addi	$sp, $sp, -4		# abre um espaço na pilha
	sw	$v1, 0($sp)		# Guarda v1
cont_esc_buff2:
	li	$a3, 2			# Carrega a flag de não pular linha, nem add espaço
	la 	$v1, labelbuffer($t0)	# Carrega o endereço de escrita
	jal 	wrtcompactfile		# Escreve no arquivo
	sw	$zero, labelbuffer($t0)	# Zera o espaço no labelbuffer
	addi	$t0, $t0, 4		# Desloca 1 word para frente
	blt	$t0, 16, cont_esc_buff2 # Continua o processo de escrita caso não tenha escrito os 16
	lw	$v1, 0($sp)		# Recupera v1
	addi	$sp, $sp, 4		# reorganiza espaço na pilha
	beq	$v1, 3, bfr2_nw		# linha terminou em \n
	move	$a3, $0			# linha terminou apenas com um ws
	j	wrtbfr2_fim		# Finaliza a escrita
bfr2_nw:
	li	$a3, 1			# Carrega a flag de nw
	addi	$s5, $s5, 4		# Corrige o endereço, a linha acabou
wrtbfr2_fim:
	la	$v1, labelbuffer($zero)	# Carrega um espaço da memoria vazio
	jal 	wrtcompactfile		# Escreve o espaço em branco
	j	p_prog_loop		# Retorna ao loop
	
wrtbfr2lbl:
	addi	$t0, $zero, 16		# Guarda 16 em $t0
	sw	$s5, labelbuffer($t0)	# Guarda o endereço no final do buffer
	jal	wrtlabelfile		# Armazena no arquivo de endereço de labels
	add	$t0, $zero, $zero	# zera t0
cont_zero:
	sw	$zero, labelbuffer($t0)	# Zera o espaço no labelbuffer
	addi	$t0, $t0, 4		# Desloca 1 word para frente
	blt	$t0, 20, cont_zero	# Continua o processo de escrita caso não tenha escrito os 20
	j	p_prog_loop		# Retorna ao loop
	

# Processador do programa, vai lendo o texto letra por letra até encontrar caracteres de controle.
# Caso encontre, processa de acordo. Os caracteres de controle são '\n', '$' e ':'
p_prog:	
	lui	$s5, 0x0040		# Carrega o endereço do inicio em $s5
	ori	$s5, 0X0000		# Carrega a parte inferior do endereço
	addi	$s4, $zero, 1		# Carrega 1 em s4 para indicar que começou o processamento da .text
p_prog_loop:
	jal 	ld_nxt			# Carrega o próximo caractere
	beq	$s3, -1, s_prog		# Não há mais caracteres
	beq	$s2, '\n', p_prog_loop	# \n depois de \n, pegar próximo
	beq	$s2, '\t', p_prog_loop	# \t, voltar ao inicio
	beq	$s2, ' ', p_prog_loop	# espaço depois de espaço
	beq	$s2, ':', p_prog_loop	# dois pontos da label que não foi lido
	beq	$s2, '#', pular_coment	# se achar um comentario, continua lendo até o fim
	slti	$t4, $s2, '#'		# Verifica se é menor ou igual a $
	sgt	$t5, $s2, '{'		# Verifica se é maior que ou igual a Z
	bne	$t4, $t5, ld_kw		# Continua
	addi	$s1, $s1, -1		# Leu algo que não deveria, voltar
ld_kw:
	move	$a3, $zero		# Dispensa a adição de \n para adicionar espaço
	jal 	ldw			# Carrega a próxima palavra
	la	$ra, prep_esc		# Prepara para voltar para a escrita, se necessário
	beq	$v0, $zero, p_prog_loop	# Caso tenha lido nada, volta ao loop, pois é um white space
	beq	$v1, 0, mesma_palavra	# Terminou de ler 4 bytes sem encontrar espaço em branco ou ,
	beq	$v1, 1, pseudo_ver	# verifica as instruções
	beq	$v1, 2, escreve_2p_in	# Pula para escrever a label
	beq	$v1, 3, esc_nova_linha	# Pula caso tenha encontrado \n	
	beq	$v1, 4, proc_reg	# Processa os registradores		
	j	prep_esc		# Se forem iguais, prepara para escrever no arquivo
	
esc_nova_linha:
	addi	$s5, $s5, 4 		# Desloca o endereço, pois acabou a linha
	li	$a3, 1			# Carrega a flag de pular linha na escrita
	andi	$t0, $v0, 0x000000ff	# Carrega apenas o primeiro caractere lido
	beq	$t0, '$', proc_reg	# É um registrador, processa ele
	j	prep_esc		# Chama a preparação para escrita
	
mesma_palavra:
	add	$t0, $zero, $zero	# zera t0
cont_mesma:
	sw	$v0, dbg
	sw	$v0, labelbuffer($t0)	# Guarda as letras no buffer2
	addi	$t0, $t0, 4		# desliza o ponteiro do buffer 2
	jal 	ldw			# Carrega a próxima word
	sw	$v0, labelbuffer($t0)	# Guarda as letras seguintes em labelbuffer
	beq 	$v1, 2, escreve_2p	# Caso tenha terminado em 2p, processa corretamente
	bgt 	$v1, $zero, wrtbfr2file # Caso tenha terminado em ws (espaço e \t) escreve tudo que foi guardado no buffer 2
	j 	cont_mesma		# Continua na mesma palavra
	
escreve_2p_in:
	add	$t0, $zero, $zero	# Zera t0 caso não acha nenhum caractere armazenado ainda
escreve_2p:
	sw	$v0, labelbuffer($t0)	# Guarda as letras no buffer2
	j	wrtbfr2lbl		# Armazena o par label endereço
		
prep_esc:
	la 	$v1, escrita		# Carrega o endereço de escrita
	sw 	$v0, escrita		# Carrega o valor a ser escrito
	jal 	wrtcompactfile		# Escreve
	j 	p_prog_loop		# Retorna ao loop

# Essa função verifica se a instrução se trata de uma pseudo instrução
pseudo_ver:
	andi	$t0, $v0, 0x000000ff	# Carrega apenas o primeiro caractere lido
	beq	$t0, '$', proc_reg	# É um registrador, processa ele
	lw	$t1, _li		# Carrega "li"
	beq	$v0, $t1, trans_li	# Verifica se o comando é li
	j 	prep_esc		# escreve	
	
	
# Esssa função transforma li nas funções que o compõe
trans_li:
	jal 	ld_nxt			# Carrega o próximo caractere
	beq	$s3, -1, exit		# Não há mais caracteres
	beq	$s2, '\n', exit		# \n depois de li(ws), invalido
	beq	$s2, '\t', trans_li	# \t depois de li(ws), voltar a carregar
	beq	$s2, ' ', trans_li	# espaço depois de li(ws), voltar a carregar
	addi	$s1, $s1, -1		# volta um espaço, leu um caractere
	jal 	ldw			# Carrega o registrador destino
	jal	proc_reg		# Processa o registrador destino
	move	$t6, $v0		# Coloca o registrador destino em t6
s_arg_li:
	jal 	ld_nxt			# Carrega o próximo caractere
	beq	$s3, -1, exit		# Não há mais caracteres
	beq	$s2, '\n', s_arg_li	# \n depois de \n, pegar próximo
	beq	$s2, '\t', s_arg_li	# \t, voltar ao inicio
	beq	$s2, ' ', s_arg_li	# espaço depois de espaço
	addi	$s1, $s1, -1		# volta um espaço
	add	$t2, $zero, $zero	# Zera t2 
carr_arg_li:
	jal 	ldw			# Carrega 4 caracteres do imediato
	sw	$v0, bufferi($t2)	# Guarda no buffer do arg
	addi	$t2, $t2, 4		# Desloca o ponteiro do buffer
	beq	$v1, $zero, carr_arg_li # Continua carregando, não acabou o imediato 
escrevendo:
	move	$a3, $zero		# Carrega espaço como terminador
	la 	$v1, escrita		# Carrega o endereço de escrita
	lw	$t3, _lui		# Carrega o lui e escreve
	sw 	$t3, escrita		# Carrega o valor a ser escrito
	jal 	wrtcompactfile		# Escreve
	lw	$t3, at_t		# Carrega o registrador auxiliar
	lbu	$t4, virg		# Carrega a virgula
	sll	$t4, $t4, 16		# Coloca a virgula na posição
	or	$t3, $t3, $t4		# Adiciona a vírgula na posição
	sw 	$t3, escrita		# Carrega o valor a ser escrito
	jal 	wrtcompactfile		# Escreve
	li	$a3, 2			# Carrega o indicador de final sem ws
	lw	$t3, zerox		# Carrega 0x para escrever
	sw 	$t3, escrita		# Carrega o valor a ser escrito
	jal 	wrtcompactfile		# Escreve
	li	$a3, 1			# Carrega a opçao de pular linha
	lbu	$t0, bufferi($zero)	# Carrega o primeiro byte do imediato
	lbu	$t1, bufferi+1		# Carrega o segundo byte do imediato
	sll	$t1, $t1, 8		# Desloca o segundo byte
	or	$t7, $t0, $t1		# Junta os dois
	lw	$t3, zerox		# Carrega 0x para comparar
	beq	$t7, $t3, em_hex	# Compara se começa com 0x
	j	loadintarg		# Pula para escrever

em_hex:
	la $t0, bufferi
	add $t0, $t0, 2
	lhu $t1, 0($t0)
	add $t0, $t0, -2
	sh $t1,0($t0)
	add $t0, $t0, 4
	lhu $t1, 0($t0)
	add $t0, $t0, -2
	sh $t1,0($t0)
	add $t0, $t0, 4
	lhu $t1, 0($t0)
	add $t0, $t0, -2
	sh $t1,0($t0)
	add $t0, $t0, 4
	lhu $t1, 0($t0)
	add $t0, $t0, -2
	sh $t1,0($t0)
	add $t0, $t0, 2
	li $t1, 0
	sh $t1,0($t0)
afterint:
	move	$t0, $zero		# zera t0
	move	$t3, $zero		# zera t3
	move	$t4, $zero		# zera t4
cont_em_hex:	
	lbu	$t2, bufferi($t0)	# Carrega um byte do imediato
	sllv	$t2, $t2, $t4		# Desloca a quantidade de bytes lidos
	addi	$t4, $t4, 8		# Ajusta o deslocamento
	or	$t3, $t3, $t2		# soma o byte
	addi	$t0, $t0, 1		# Soma 1 para percorrer o array
	blt	$t0, 6, cont_em_hex	# Verifica se carregou todos
escreve_ui:	
	sw 	$t3, escrita		# Carrega o valor a ser escrito
	sw	$t3, dbg
	jal 	wrtcompactfile		# Escreve

	move	$a3, $zero		# Carrega espaço como terminador
	la 	$v1, escrita		# Carrega o endereço de escrita
	lw	$t3, _ori		# Carrega o ori e escreve
	sw 	$t3, escrita		# Carrega o valor a ser escrito
	jal 	wrtcompactfile		# Escreve
	sw 	$t6, escrita		# Carrega o valor a ser escrito
	sw	$t6, dbg		# Verificar
	jal 	wrtcompactfile		# Escreve
	lw	$t3, at_t		# Carrega o registrador auxiliar
	lbu	$t4, virg		# Carrega a virgula
	sll	$t4, $t4, 16		# Coloca a virgula na posição
	or	$t3, $t3, $t4		# Adiciona a vírgula na posição
	sw 	$t3, escrita		# Carrega o valor a ser escrito
	jal 	wrtcompactfile		# Escreve
	li	$a3, 2			# Carrega o indicador de final sem ws
	lw	$t3, zerox		# Carrega 0x para escrever
	sw 	$t3, escrita		# Carrega o valor a ser escrito
	jal 	wrtcompactfile		# Escreve
	li	$a3, 1			# Carrega a opçao de pular linha
	lw	$t3, zerox		# Carrega 0x para escrever
	j em_hex2

loadintarg:
	la $t0, bufferi
	la	$t3, writingbuffer
	sw $zero, 0($t3)
	sw $zero, 4($t3)
ldintargloop:
	lbu $t7 ,0($t0)
	beq $t7, '\0', callcvtint
	beq $t7, '\n', callcvtint
	beq $t7, '\r', callcvtint
	beq $t7, ' ', callcvtint
	sb $t7, 0($t3)
	addi $t3, $t3, 1
	addi $t0, $t0, 1
	j ldintargloop
callcvtint:
	la 	$t3, writingbuffer
	jal convertint
	la 	$t3, writingbuffer
	jal converthex
	la  $t3, writingbuffer
	lw $t3, 0($t3)
	sw $t3, bufferi($zero)
	la  $t3, writingbuffer
	lw $t3, 4($t3)
	li $t0, 4
	sw $t3, bufferi($t0)
	li $t0, 0
	j afterint

em_hex2:
	addi	$t0, $zero, 4		# Inicia lendo os bytes do imediato
	move	$t3, $zero		# zera t3
	move	$t4, $zero		# zera t4		# zera t3
cont_em_hex2:	
	lbu	$t2, bufferi($t0)	# Carrega um byte do imediato
	sllv	$t2, $t2, $t4		# Desloca a quantidade de bytes lidos
	addi	$t4, $t4, 8		# Ajusta o deslocamento
	or	$t3, $t3, $t2		# soma o byte
	addi	$t0, $t0, 1		# Soma 1 para percorrer o array
	blt	$t0, 10, cont_em_hex2	# Verifica se carregou todos
escreve_ui2:	
	sw 	$t3, escrita		# Carrega o valor a ser escrito
	jal 	wrtcompactfile		# Escreve
	addi 	$s5, $s5, 8		# Corrige o endereço
	j 	p_prog_loop

proc_reg:
	andi	$t0, $v0, 0x000000ff	# Carrega apenas o primeiro caractere lido
	bne	$t0, '$', prep_esc	# Não é um registrador, escreve logo
	andi	$t3, $v0, 0xff000000	# Carrega os bytes que não fazem parte do registrador
	andi	$t0, $v0, 0x00ffffff	# Separa apenas os caracteres $xx
	move	$t2, $0			# Zera t2	
cont_proc_reg:		
	lw	$t1, at($t2)		# Carrega a mascara de registrador
	beq	$t0, $t1, sub_reg	# Parte para substituir o registrador, caso tenha encontrado a equivalência
	addi	$t2, $t2, 8		# Pula para a próxima mascará de registrador
	beq	$t2, 248, prep_esc	# Testou todos os registradores, escreve e continua
	j 	cont_proc_reg		# Continua procurando o equivalente
sub_reg:
	addi	$t2, $t2, 4		# Carrega o endereço da mascara traduzida do reg
	lw	$v0, at($t2)		# Carrega o nome sem mascara do registrador
	lbu	$t0, virg		# Carrega a vírgula
	sll	$t0, $t0, 24		# Desloca ela para o último byte
	or	$v0, $v0, $t3		# Carrega os bytes superiores novamente
	sw	$v0, dbg2
	jr	$ra			# retorna para onde foi chamado
	

# Essa função processa o arquivo temporário	
s_prog:
	jal 	wrttxtfile		# Inicia a escrita do arquivo text.mif
	li   	$v0, 16       		# Fecha arquivo da funcao wrtcompactfile
	move 	$a0, $s0      		# Codigo do arquivo a ser fechado
	syscall            		# Syscall -> Fechar arquivo
	move	$s5, $zero		# Zera s5, endereço
	lui	$s5, 0x0040		# Carrega o endereço do inicio em $s5
	ori	$s5, 0X0000		# Carrega a parte inferior do endereço
	addi	$s5, $s5, -4		# Ajuste para iniciar do começo
	move	$s3, $zero		# Zera s3
	move	$s1, $zero		# Zera s1
	jal 	Abrir2			# Abre o arquivo temporário
s_prog_loop:
	beq	$s3, -1, exit_ok	# Não há mais caracteres
	jal	ld_nxt			# Carrega 1 caractere
	slti	$t4, $s2, 'A'		# Verifica se é menor ou igual a $
	sgt	$t5, $s2, 'z'		# Verifica se é maior que ou igual a Z
	bne	$t4, $t5, s_prog_loop	# Offset é negativo
	addi	$s1, $s1, -1		# Volta 1 caractere
	jal 	ldw			# Carrega a instrução
	andi	$t1, $v0, 0x000000ff	# Carrega o primeiro caracter
	addi	$t1, $zero, -12		# Carrega -12 em t1
	addi	$s5, $s5, 4		# Avança o contador em 4 unidades
cont_s_prog:
	beq	$s3, -1, exit_ok	# Não há mais caracteres
	beq	$t1, 360, exit		# Instrução inválida
	addi	$t1, $t1, 12		# Avança o contador
	la	$t0, _add($t1)		# Carrega o endereço da instrução possível
	lw	$t2, ($t0)		# Carrega a instrução
	bne	$v0, $t2, cont_s_prog	# Caso não seja a instrução analisada, confere a próxima
	addi	$t3, $t0, 8		# Carrega o endereço do tipo de instrução	
	lw	$t3, ($t3)		# Carrega o tipo de instrução
	beq	$t3, $zero, int_r	# Processa se for instrução do tipo r
	beq	$t3, 1, int_w		# Processa lw e sw
	beq	$t3, 2, int_j		# Processa j e jal
	beq	$t3, 3, int_jr		# Processa se for jr
	beq	$t3, 4, int_b		# Processa se for b
	beq	$t3, 5, int_lui		# Processa se for lui
	beq	$t3, 6, shift		# Processa sll e srl
	beq	$t3, 7, int_i		# Processa instruções do tipo i
	beq	$t3, 8, mult_div	# Processa se for multiplicação ou divisão
	beq	$t3, 9, mfa		# Processa mfhi, mflo
	beq	$t3, 10, int_bgez	# Processa bgez
	beq	$t3, 11, int_clo	# Processa se a instrução for clo
	
int_w:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	jal 	ldw			# Carrega o registrador 1
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 16			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	move	$k0, $zero		# Zera k0
	move	$k1, $zero		# Zera k1
search_offset:
	jal	ld_nxt			# Carrega o próximo caractere
	slti	$t4, $s2, '0'		# Verifica se é menor ou igual a $
	sgt	$t5, $s2, '9'		# Verifica se é maior que ou igual a Z
	beq	$s2, '-', off_neg	# Offset é negativo
	bne	$t4, $t5, search_offset	# Continua procurando o começo do imediato
read_offset:
	sb	$s2, writingbuffer($k0)	# Guarda um byte
	addi	$k0, $k0, 1		# Incrementa o contador
	jal	ld_nxt			# Lê um caractere
	bne	$s2, '(', read_offset	# Continua lendo até achar (
	la	$t3, writingbuffer	# Carrega o endereço do offset
	sw	$t0, dbg		# Guarda a instrução
	jal 	convertint		# Converte o offset em um inteiro
	lw	$t0, dbg		# Recupera a instrução
	lw	$v0, writingbuffer($0)	# Carrega o inteiro no registrador
	sw	$v0, dbg
	beq	$k1, 1, treat_neg	# Caso seja negativo
fim_w:
	andi	$v0, $v0, 0x0000ffff	# Mascara os bits superiores
	or	$t0, $t0, $v0		# Adiciona o offset ao comando
	jal 	ldw			# Carrega o registrador base
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 21			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	sw	$t0, instructionpair	# Coloca no buffer para escrever
	sw	$s5, instructionpair+4	# Coloca no buffer para escrever
	jal	wrttxt			# Grava na saida
	j	s_prog_loop		# Continua o loop
	
treat_neg:
	not	$v0, $v0		# Inverte
	addi	$v0, $v0, 1		# Soma 1 em v0, complemento a dois
	j	fim_w			# Finaliza a funcao	
	
off_neg:
	addi	$k1, $zero, 1		# Define uma flag para marcar o offset como negativo
	jal	ld_nxt			# Carrega o primeiro número
	j	read_offset		# Volta para ler o offset
	
	
int_b:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	jal 	ldw			# Carrega o registrador 1
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 21			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	jal 	ldw			# Carrega o registrador 1
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 16			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	j 	limp			# Carrega a label como o bgez
	
int_bgez:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	jal 	ldw			# Carrega o registrador 1
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 21			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
limp:
	jal	ld_nxt			# Lê um caractere
	slti	$t4, $s2, '0'		# Verifica se é menor ou igual a $
	sgt	$t5, $s2, '{'		# Verifica se é maior que ou igual a Z
	bne	$t4, $t5, limp		# Continua
	addi	$s1, $s1, -1		# Leu algo que não deveria, voltar
	jal	carr_lbl		# Carrega a label
	sw	$t0, dbg		# guarda t0(instrução) na memoria
	jal 	buscar_lbl		# Busca o endereço da label
	lw	$v0, writingbuffer($0)	# Carrega o endereço da label
	subu	$v0, $v0, $s5		# Calcula o offset
	addi	$v0, $v0, -4		# Ainda não foi incrementado $s5
	sw	$v0, dbg2
	srl	$v0, $v0, 2		# Desliza
	andi	$v0, $v0, 0x0000ffff	# Mascara apenas a parte necessária
	lw	$t0, dbg		# Carrega a instrução de volta
	or	$t0, $t0, $v0		# Carrega o offset do jump na instrução
	sw	$t0, instructionpair	# Coloca no buffer para escrever
	sw	$s5, instructionpair+4	# Coloca no buffer para escrever
	jal	wrttxt			# Grava na saida
	j	s_prog_loop		# Continua o loop
	
	
int_j:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	jal 	carr_lbl		# Carrega a label 
	sw	$t0, dbg		# guarda t0 na memoria
	jal 	buscar_lbl		# Busca o endereço da label
	lw	$v0, writingbuffer($0)	# Carrega o endereço da label
	andi	$v0, 0x03ffffff		# Pega apenas a parte necessária
	srl	$v0, $v0, 2		# Desloca a label para a direita
	lw	$t0, dbg		# Carrega a instrução de volta
	or	$t0, $t0, $v0		# Carrega o endereço do jump na instrução
	sw	$t0, instructionpair	# Coloca no buffer para escrever
	sw	$s5, instructionpair+4	# Coloca no buffer para escrever
	jal	wrttxt			# Escreve no arquivo
	j	s_prog_loop		# Continua o loop
		
carr_lbl:
	addi	$sp, $sp, -4		# Abre um espaço na pilha
	sw	$ra, 0($sp)		# Guarda ra na pilha
	addi 	$k1, $zero, -4		# Inicializa k1
cont_carr:
	jal 	ldw			# Carrega o começo da label
	addi	$k1, $k1, 4		# Desliza ponteiro do buffer
	sw	$v0, labelbuffer($k1)	# Carrega a primeira word da label em labelbuffer
	beq	$v1, $zero, cont_carr	# Continua carregando
fim_carr_lbl:
	lw	$ra, 0($sp)		# recupera ra da pilha
	addi	$sp, $sp, 4		# Retorna a pilha para a posição
	jr	$ra			# Retorna para caller	
	
buscar_lbl:
	addi	$sp, $sp, -12
	sw	$ra, 0($sp)
	sw	$t8, 4($sp)
	sw	$t9, 8($sp)
	li 	$v0, 13	
	la 	$a0, labelfile     	# Carrega o nome do arquivo
	li	$a1, 0       	 	# flag da leitura (0 leitura, 1 escrita)
	li 	$a2, 0        		# mode é ignorado
	syscall 
	move 	$s7, $v0 	 	# Salva o código do arquivo
	
buscando:
	li	$k0, -1			# Zera k0
cont_buscando:	
	beq	$k0, 3, fim_busc
	addi	$k0, $k0, 1
	li   	$v0, 14      		# Carrega o código de leitura de arquivo
	move 	$a0, $s7		# Código do arquivo 
	la   	$a1, ld_buffer($k0)   	# endereço do buffer
	li  	$a2,  1      		# tamanho do buffer
	syscall      			# Lê do arquivo
	blez	$v0, exit		# Não achou a label, deu bosta
	lbu	$v1, ld_buffer($k0)	# Carrega o caractere
	slti	$t8, $k0, 3		# Já leu 4?
	sne	$t9, $v1, '\t'		# Leu \t?	 
	beq	$t8, $t9, cont_buscando
fim_busc:
	bne	$t9, $zero, sem_s	
	sb	$zero, ld_buffer($k0)
sem_s:
	lw	$v0, ld_buffer		# Carrega o q leu do arquivo em $v0
	lw	$v1, labelbuffer	# Carrega o começo da label
	beq	$v0, $v1, poss_lbl	# Caso comecem iguais,
	j n_igual
	
poss_lbl:
	move	$k0, $zero
	sw	$v1, labelbuffer2($k0)
	addi	$k0, $k0, 3
prox_poss:
	addi	$k0, $k0,1
	bge	$k0, $k1, lbl_igual	# Encontrou
	li   	$v0, 14      		# Carrega o código de leitura de arquivo
	move 	$a0, $s7		# Código do arquivo 
	la   	$a1, ld_buffer   	# endereço do buffer
	li  	$a2,  1      		# tamanho do buffer
	syscall  
	blez	$v0, exit		# Erro na leitura ou leitura 0
	lbu	$v0, ld_buffer		# Carrega o byte lido
	sb	$v0, labelbuffer2($k0)	# Armazena o caractere
	lb	$v1, labelbuffer($k0)	# Busca o caractere da label sendo buscada
	beq	$v0, $v1, prox_poss	# Busca mais 1 caractere
n_igual:
	sw	$zero, ld_buffer($0)
	li   	$v0, 14      		# Carrega o código de leitura de arquivo
	move 	$a0, $s7		# Código do arquivo 
	la   	$a1, ld_buffer   	# endereço do buffer
	li  	$a2,  1      		# tamanho do buffer
	syscall
	lbu	$v0, ld_buffer		# Carrega o byte lido
	beq	$v0, '\n', buscando	# n era igual, vamos ler a proxima
	j	n_igual			# lê de 1 em 1 até achar o \n
lbl_igual:
	li   	$v0, 14      		# Carrega o código de leitura de arquivo
	move 	$a0, $s7		# Código do arquivo 
	la   	$a1, ld_buffer   	# endereço do buffer
	li  	$a2,  1      		# tamanho do buffer
	syscall
	lbu	$v0, ld_buffer		# Carrega o byte lido
	bne	$v0, 'x', lbl_igual	# Continua procurando o x
	li   	$v0, 14      		# Carrega o código de leitura de arquivo
	move 	$a0, $s7		# Código do arquivo 
	la   	$a1, add_buffer   	# endereço do buffer
	li  	$a2,  8      		# tamanho do buffer
	syscall
	li   	$v0, 16       		# Fecha arquivo da funcao wrtdata
	move 	$a0, $s7      		# Codigo do arquivo a ser fechado
	syscall            		# Syscall -> Fechar arquivo
	la	$t3, add_buffer		# Carrega o endereco do endereco ascii hexa
	jal	convertaschex		# Converte
	lw	$t8, 4($sp)
	lw	$t9, 8($sp)
	lw	$ra, 0($sp)		# Recupera
	addi	$sp, $sp, 12		# Ajeita endereço
	jr	$ra			# Retorna

	
int_i:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	jal 	ldw			# Carrega o registrador 1
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 16			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	jal 	ld_nxt			# Carrega um char
	beq	$s2, '\t', continuar_i	# \t, voltar ao inicio
	beq	$s2, ' ', continuar_i	# espaço depois de espaço
	beq	$s2, ',', continuar_i	# espaço depois de espaço
	addi	$s1, $s1, -1
	
continuar_i:
	jal 	ldw			# Carrega o registrador 2
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 21			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	sw	$t0, dbg		# Guarda na memória a mascara
	la	$t3, writingbuffer	# Coloca o endereço da entrada
limpa_buff:
	jal 	ld_nxt			# Carrega um char
	beq	$s2, '\t', limpa_buff	# \t, voltar ao inicio
	beq	$s2, ' ', limpa_buff	# espaço depois de espaço
	beq	$s2, ',', limpa_buff	# espaço depois de espaço
	addi	$s1, $s1, -1
		
cont_i_imm:
	jal 	carr_im			# Carrega o imediato
	sw	$t0, dbg		# Guarda na memória a mascara
	lw	$t1, writingbuffer($0)	#Recupera o imediato
	or	$t0, $t0, $t1		# Adiciona imediato
	sw	$t0, instructionpair	# Coloca no buffer para escrever
	sw	$s5, instructionpair+4	# Coloca no buffer para escrever
	jal	wrttxt			# Escreve no arquivo
	j	s_prog_loop		# Continua o loop
	
int_lui:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	sw	$t0, dbg
	jal 	ldw			# Carrega o registrador 1
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 16			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	sw	$t0, dbg		# Guarda na memória a mascara
	la	$t3, writingbuffer	# Coloca o endereço da entrada
	jal 	carr_im			# Carrega o imediato
	sw	$t0, dbg		# Guarda na memória a mascara
	lw	$t1, writingbuffer($zero)#Recupera o imediato
	or	$t0, $t0, $t1		# Adiciona imediato
	sw	$t0, instructionpair	# Coloca no buffer para escrever
	sw	$s5, instructionpair+4	# Coloca no buffer para escrever
	jal	wrttxt			# Escreve no arquivo
	j	s_prog_loop		# Continua o loop
	
carr_im:
	addi	$sp, $sp, -8
	sw	$ra, 0($sp)
	sw	$t0, 4($sp)
	jal 	ldw			# Carrega o imediato
	move	$t2, $zero		# Zera t2
	sw	$v0, writingbuffer($t2)	# Guarda a primeira parte do imediato
	bne	$v1, $zero, comp_int	# O imediato tem mais de 4 caracteres
	addi	$t2, $t2, 4		# Desloca a posição do buffer
	jal 	ldw			# Carrega o resto do imediato
	sw	$v0, writingbuffer($t2)	# Guarda a segunda parte do imediato
	lw	$t2, writingbuffer($0)	# Carrega a primeira parte do imediato
	andi	$t2, $t2, 0x0000ffff	# Carrega os dois primeiros caracteres
	lw	$t4, zerox		# Carrega 0x
	bne	$t2, $t4, comp_int	# Não está em hexa, não começa com 0x
	la	$t3, writingbuffer+2	# Carrega o endereço do conteúdo em hexa
	jal	convertaschex		# Converte o ascii hexa para hexa
	j	fim_carr_im		# Finaliza
comp_int:
	lbu	$t2, writingbuffer($0)	# Carrega o primeiro byte do imediato
	beq	$t2, '-', int_neg	# Processa número negativo
	jal 	convertint		# Converte o inteiro em ascii para guardar no reg
fim_carr_im:
	lw	$ra, 0($sp)
	lw	$t0, 4($sp)
	addi	$sp, $sp, 8
	jr	$ra
int_neg:
	la	$t3, writingbuffer+1	# Converte o inteiro como se fosse positivo
	jal 	convertint		# Converte o inteiro em ascii para guardar no reg
	lw	$t4, writingbuffer($0)	# Pega o valor convertido
	not	$t4, $t4		# inverte
	addi	$t4, $t4, 1		# Soma 1, complemento a 2
	andi	$t4, $t4, 0x0000ffff	# Separa apenas 16 bits
	sw	$t4, dbg
	sw	$t4, writingbuffer($0)	# Armazena o número como negativo
	j	fim_carr_im		# Finaliza
	
shift:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	jal 	ldw			# Carrega o registrador 1
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 11			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	jal 	ldw			# Carrega o registrador 2
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 16			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	jal 	ldw			# Carrega o imediato
	sw	$t0, dbg		# Guarda na memória a mascara
	sw	$v0, writingbuffer($zero)#Coloca o argumento na entrada
	la	$t3, writingbuffer	# Coloca o endereço da entrada
	jal 	convertint		# Converte o inteiro em ascii para guardar no reg
	lw	$t0, dbg		# Recupera a mascara da memoria
	lw	$t1, writingbuffer($zero)#Recupera o inteiro
	sll	$t1, $t1, 6		# Desliza shamt para posição
	or	$t0, $t0, $t1		# Adiciona shamt
	sw	$t0, instructionpair	# Coloca no buffer para escrever
	sw	$s5, instructionpair+4	# Coloca no buffer para escrever
	jal	wrttxt			# Escreve no arquivo
	j	s_prog_loop		# Continua o loop
	
	
int_jr:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	jal 	ldw			# Carrega o registrador 1
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 21			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	sw	$t0, instructionpair	# Coloca no buffer para escrever
	sw	$s5, instructionpair+4	# Coloca no buffer para escrever
	jal	wrttxt			# Escreve no arquivo
	j	s_prog_loop		# Continua o loop
	
mfa:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	jal 	ldw			# Carrega o registrador 1
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 11			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	sw	$t0, instructionpair	# Coloca no buffer para escrever
	sw	$s5, instructionpair+4	# Coloca no buffer para escrever
	jal	wrttxt			# Escreve no arquivo
	j	s_prog_loop		# Continua o loop

	
int_clo:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	jal 	ldw			# Carrega o registrador 1
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 11			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	sw	$t0, dbg
	jal 	ldw			# Carrega o registrador 2
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 21			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	sw	$t0, instructionpair	# Coloca no buffer para escrever
	sw	$s5, instructionpair+4	# Coloca no buffer para escrever
	jal	wrttxt			# Escreve no arquivo
	j	s_prog_loop		# Continua o loop
	

mult_div:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	jal 	ldw			# Carrega o registrador 1
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 21			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	sw	$t0, dbg
	jal 	ldw			# Carrega o registrador 2
	jal	reg_ag			# Trata o primeiro registrador
	li	$a0, 16			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	sw	$t0, instructionpair	# Coloca no buffer para escrever
	sw	$s5, instructionpair+4	# Coloca no buffer para escrever
	jal	wrttxt			# Escreve no arquivo
	j	s_prog_loop		# Continua o loop
	
# Processa instruções do tipo R
int_r:
	lw	$t0, 4($t0)		# Carrega a máscara da instrução
	jal 	ldw			# Carrega o registrador destino
p_reg_r:
	jal 	reg_ag			# processa o primeiro registrador
	li	$a0, 11			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
s_reg_r:
	jal 	ldw			# Carrega o segundo registrador
	jal	reg_ag			# processa o registrador
	li	$a0, 21			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador	
t_reg_r:
	jal 	ldw			# Carrega o segundo registrador
	jal	reg_ag			# processa o registrador
	li	$a0, 16			# Carrega o offset do registrador
	jal 	add_reg			# Mescla a mascara com o registrador
	sw	$t0, instructionpair	# Coloca no buffer para escrever
	sw	$s5, instructionpair+4	# Coloca no buffer para escrever
	jal	wrttxt			# Escreve no arquivo
	j	s_prog_loop		# Continua o loop
	

reg_lt_dez:
	andi	$v0, $v0, 0x000000ff	# Salva apenas o número
	lw	$ra, 0($sp)		
	addi	$sp, $sp, 4
	jr	$ra			# retorna para processo
lbl_zero:
	li	$v0, 0x00000030		# Carrega '0' no resultado
limp_zero:
	jal 	ld_nxt			# Carrega um caractere
	slti	$t4, $s2, '#'		# Verifica se é menor ou igual a 0
	sgt	$t5, $s2, 'z'		# Verifica se é maior que ou igual a z
	beq	$t4, $t5, limp_zero	# Continua procurando o começo do imediato
	addi	$s1, $s1, -1		# Leu caracteres a mais
	lw	$ra, 0($sp)		
	addi	$sp, $sp, 4
	jr	$ra			# retorna para processo

add_reg:
	addi	$sp, $sp, -4		# Abre espaço na pilha
	sw	$ra, 0($sp)		# Guarda na pilha
	sw	$t0, dbg		# Guarda na memória a mascara
	sw	$v0, writingbuffer($zero)#Coloca o argumento na entrada
	la	$t3, writingbuffer	# Coloca o endereço da entrada
	jal 	convertint		# Converte o inteiro em ascii para guardar no reg
	lw	$t0, dbg		# Recupera a mascara da memoria
	lw	$t1, writingbuffer($zero)#Recupera o inteiro
	sllv	$t1, $t1, $a0		# Desliza ele para a posição
	or	$t0, $t0, $t1		# Carrega ele na instrução
	lw	$ra, 0($sp)		# Guarda na pilha
	addi	$sp, $sp, 4		# Abre espaço na pilha
	jr	$ra

slide_reg_ag:
	srl	$v0, $v0, 8		# Move o valor para a base do registrador e exclui um caractere
	j	cont_reg_ag		# Continua
reg_ag:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)		
cont_reg_ag:
	sw	$v0, dbg
	andi	$t1, $v0, 0x000000ff	# Pega apenas o primeiro
	bne	$t1, '$', slide_reg_ag	# Continua fazendo o shift para achar o $
	andi	$t1, $v0, 0x0000ff00	# Carrega o segundo caractere
	srl	$t1, $t1, 8		# move ele para a base
	slti	$t4, $t1, 'A'		# Verifica se é menor ou igual a A
	sgt	$t5, $t1, 'y'		# Verifica se é maior que ou igual a z
	la	$ra, reg_ok
	beq	$t4, $t5, proc_reg	# Verifica se são iguais
reg_ok:
	srl	$v0, $v0, 8		# Move o valor para a base do registrador e exclui $
	andi	$t1, $v0, 0x000000ff	# Pega apenas o primeiro
	beq	$t1, 'z', lbl_zero	# label zero
	andi	$t1, $v0, 0x0000ff00	# Carrega o segundo caractere
	srl	$t1, $t1, 8		# move ele para a base
	slti	$t4, $t1, '0'		# Verifica se é menor ou igual a 0
	sgt	$t5, $t1, '9'		# Verifica se é maior que ou igual a 9
	bne	$t4, $t5, reg_lt_dez	# Registrador é menor q 10
	andi	$v0, $v0, 0x0000ffff	# Salva apenas o número
	lw	$ra, 0($sp)	
	addi	$sp, $sp, 4
	jr	$ra

#Escreve o arquivo que contém as labels e seus respectivos endereços
wrtlabelfile:
	addi 	$sp, $sp, -32		# Abre espaco na pilha
	sw 	$a0, 0($sp)		# Salva -> Word a ser escrita
	sw 	$a1, 4($sp)		# Salva -> Endereco 
	sw 	$a2, 8($sp)		# Salva -> valor contido em $a2
	sw 	$v0, 12($sp)		# Salva -> valor anterior de $v0
	sw 	$t0, 16($sp)		# Salva -> valor anterior de $t0
	sw 	$t1, 20($sp)		# Salva -> valor anterior de $t1
	sw	$ra, 28($sp)		# Salva -> valor anterior de $ra
	la	$t0, labelbuffer	#Carrega o endereço do buffer de escrita
	li 	$t1, 0			#Zera $t0 para servir como contador
count:
	lbu	$t2, 0($t0)		#Carrega o primeiro byte
	addi	$t1, $t1, 1		#Adiciona 1 ao contador de caracteres
	addi	$t0, $t0, 1		#Desloca o ponteiro
	bne	$t2, '\0', count	#Verifica se é '\0' para parar  de contar
	addi 	$t1, $t1, -1		#Correcao de contagem devido a forma de contagem
	la	$t0, labelbuffer	#Carrega o endereço do buffer de escrita
	li   	$v0, 13       		# Diretiva -> Abrir arquivo
  	la   	$a0, labelfile   	# Nome do arquivo de saída
  	li   	$a1, 9        		# Modo escrita
  	li   	$a2, 0        		# Ignorar modo
  	syscall            		# Syscall -> Abrir arquivo

	move	$a1, $t0		# Carrega o endereço que contém o byte a ser escrito
	move 	$a2, $t1		# Numero de caracteres a serem escritos
	move	$t1, $v0		# Salva o codigo do arquivo para nao ser perdido durante execucao
	move	$a0, $v0		# Codigo do arquivo que sera escrito
	li 	$v0 , 15		# Diretiva-> Escrever no arquivo
	syscall				# Syscall-> Escrever no arquivo
	la	$a1, tab		# Carrega o endereço que contém o byte a ser escrito
	li 	$a2, 1			# Numero de caracteres a serem escritos
	li 	$v0 , 15		# Diretiva-> Escrever no arquivo
	syscall				# Syscall-> Escrever no arquivo
	
	la	$a1, zerox		# Carrega o endereço que contém o byte a ser escrito
	li 	$a2, 2			# Numero de caracteres a serem escritos
	li 	$v0 , 15		# Diretiva-> Escrever no arquivo
	syscall				# Syscall-> Escrever no arquivo

	la  	$t3, writingbuffer	# Carrega o endereço do buffer de escrita
	lw	$t3, 0($t3)
	sw 	$t3, 24($sp)		# Salva -> valor anterior de $t1
	la  	$t3, writingbuffer	# Carrega o endereço do buffer de escrita
	lw	$t1, 16($t0)		#Carrega o elemento referente ao endereço
	sw	$t1, 0($t3)		#Armazena no buffer que ira fazer a conversão
	jal	converthex

	la	$a1, writingbuffer	# Carrega o endereço que contém o byte a ser escrito
	li 	$a2, 8			# Numero de caracteres a serem escritos
	li 	$v0 , 15		# Diretiva-> Escrever no arquivo
	syscall				# Syscall-> Escrever no arquivo
	
	la	$a1, new_line           # Carrega o endereço que contém o byte a ser escrito
	li 	$a2, 1			# Numero de caracteres a serem escritos
	li 	$v0 , 15		# Diretiva-> Escrever no arquivo
	syscall				# Syscall-> Escrever no arquivo
	
	li   	$v0, 16       		# Fecha arquivo da função 
	syscall        			# Syscall -> Fechar arquivo

	lw	$t3, 24($sp)		# Retorna ao valor anterior
	lw	$a0, 0($sp)		# Retorna ao valor anterior
	lw	$a1, 4($sp)		# Retorna ao valor anterior 
	lw	$a2, 8($sp)		# Retorna ao valor anterior
	lw	$v0, 12($sp)		# Retorna ao valor anterior
	lw	$t0, 16($sp)		# Retorna ao valor anterior
	lw	$t1, 20($sp)		# Retorna ao valor anterior
	lw	$ra, 28($sp)		# Retorna ao valor anterior
	addi 	$sp, $sp, 32		# Remove elementos da pilha
	jr $ra


#Essa função escreve em um arquivo temporário 1(word) em ASCII, deve receber como argumento $v1 que deve conter o endereço do que
#será escrito, logo o que deve ser escrito deve estar na memória previamente, não pode receber apenas um valor em um registrador 
#devido as limitações do syscall, caso seja necessário escreve em uma nova linha $s5 deve receber o valor 1.  
wrtcompactfile:
	li 	$t2, 0			# Set $t2 em 0 para comecar a escrever novamente
	addi 	$sp, $sp, -20		# Abre espaco na pilha
	sw 	$a0, 0($sp)		# Salva -> Word a ser escrita
	sw 	$a1, 4($sp)		# Salva -> Endereco 
	sw 	$a2, 8($sp)		# Salva -> valor contido em $a2
	sw 	$v0, 12($sp)		# Salva -> valor anterior de $v0
	sw	$t0, 16($sp)		# Salva -> valor anterior de $t0
	li   	$v0, 13       		# Diretiva -> Abrir arquivo
  	la   	$a0, compactfile   	# Nome do arquivo de saida
  	li   	$a1, 9        		# Modo escrita
  	li   	$a2, 0        		# Ignorar modo
  	syscall            		# Syscall -> Abrir arquivo
  	move	$t1, $v0		# Salva o codigo do arquivo para nao ser perdido durante execucao
	move 	$t3, $v1		# Carrega o endereco do buffer
keepwriting:
	lbu	$t4, 0($t3)		# Carrega o elemento na na base do buffer
	beq	$t4, '\0', next
	beq	$t4, '(', writecfile	# escreve (
	beq	$t4, ')', writecfile	# escreve )
	beq	$t4, '-', writecfile	# escreve )
	slti	$t0, $t4,0x61		# Verifica se e menor que 'a'
	beq 	$t0, 1, checkifnumber	# Se for, verificar se e numero
	slti	$t0, $t4, 0x7b		# Se nao for, verificar se pertence a [a-z]
	beq	$t0, 1 , writecfile	# Se pertencer a [a-z], escrever no arquivo.
checkifnumber:
	slti	$t0, $t4,0x30		# Verifica se e menor que '0'
	beq 	$t0, 1, checkifcomma	# Se for, verificar se e virgula.		
	slti	$t0, $t4, 0x3a		# Se nao for, verifica se pertence a [0-9]
	beq	$t0, 1, writecfile	# Se pertencer ao intervalo [0-9], escrever no arquivo.
checkifcomma:
	slti 	$t0, $t4, 0x2c		# Verifica se e menor que ','.
	beq 	$t0, 1, checkifdolar	# Se for, verificar se e '$'
	slti 	$t0, $t4, 0x2d		# Verifica se e igual a ','
	beq	$t0, 1, writecfile	# Se for, escrever no arquivo.
checkifdolar:
	slti	$t0, $t4, 0x24		# Verifica se e menor que '$'
	beq	$t0, 1, finalcheck  	# Se for, verifica se e espaco.
finalcheck:
	slti	$t0, $t4, 0x25		# Verifica se e igual a '$'
	bne	$t0, 1, next		# Se nao for, retornar
writecfile:
	move	$a1, $t3		# Carrega o endereco que contem o byte a ser escrito
	li 	$a2, 1			# Numero de caracteres a serem escritos
	move	$a0, $v0		# Codigo do arquivo que sera escrito
	li 	$v0 , 15		# Diretiva-> Escrever no arquivo
	syscall				# Syscall-> Escrever no arquivo
next:
	addi 	$t2, $t2, 1		# Adicionando 1(um) ao contador para verificar termino da palavra.
	addi	$t3, $t3, 1		# Move $t3 1(um) byte a "direita" para continuar escrevendo
	slti 	$t0, $t2, 4		# Verifica se ja escreveu 4 bytes
	move 	$v0, $t1		# Retorna a $v0 o codigo do arquivo que sera escrito
	beq	$t0, 1, keepwriting	# Se nao escreveu os 4 bytes, voltar e continuar escrevendo
	move	$a0, $t1		# Se ja escreveu os 4 bytes e n�o necessita de '\n' , $a0->Codigo do arquivo que sera escrito
	beq	$a3, 1, nova_line	# Se tiver a flag de final de linha, pula para new line
	beq	$a3, 2, return		# Se tiver a flag de palavra incompleta, pula escritas
espaco:
	la	$a1, space		# Adiciona endereco de ' ' para ser escrito
	j	escrever		# Continua
nova_line:
	la	$a1, new_line		# Adiciona o endereço de '\n' para ser escrito
escrever:	
	li 	$a2, 1			# Numero de caracteres a serem escritos
	li 	$v0, 15			# Diretiva -> Escrever no arquivo
	syscall				# Syscall -> Escrever no arquivo
return:
	li   	$v0, 16       		# Fecha arquivo da funcao wrtcompactfile
	move 	$a0, $t1      		# Codigo do arquivo a ser fechado
	syscall            		# Syscall -> Fechar arquivo
	lw 	$a0, 0($sp)		# Retorna ao valor original.
	lw 	$a1, 4($sp)		# Retorna ao valor original.
	lw 	$a2, 8($sp)		# Retorna ao valor original.
	lw 	$v0, 12($sp)		# Retorna ao valor original.
	lw	$t0, 16($sp)		# Retorna ao valor original.
	addi	$sp, $sp, 20		# Retira 4  elementos da pilha.
	jr 	$ra			# Retorna ao caller.

#Funcao que escreve arquivo text.mif
wrttxtfile:
	li   	$v0, 13       		# Deretiva -> Abrir arquivo
  	la   	$a0, textmif   		# Nome do arquivo de saida
  	li   	$a1, 9        		# Modo escrita
  	li   	$a2, 0        		# Ignorar modo
  	syscall           		# Syscall -> Abrir arquivo
  	move 	$s6, $v0		# Salva endereço em $s6
	j wrttxtheader
wrttxt:
	addi	$sp, $sp, -8		# Abre um espaço na pilha
	sw	$s0, 0($sp)		# Guarda s0
	sw 	$ra, 4($sp)		# Guarda ra
	la   	$s0, instructionpair	# Carrega o endereço de instruction pair
	addi 	$s0, $s0, 4 		# Desloca o ponteiro para escrever endereço
	lw 	$s0, 0($s0)		# Carrega o endereço
	sw 	$s0, writingbuffer($zero)# Carrega no buffer de escrita o endereço
	la	$t3, writingbuffer	# Carrega o endereço do buffer de escrita em t3
	jal 	converthex		# Chama funcao de converter para hexadecimal o numero do contador
	li	$v0, 15			# Diretiva-> Escrever no arquivo
	move	$a0, $s6		# Carrega o código do arquivo em a0
	la 	$a1, writingbuffer	# Carrega endereco do buffer que contem o que sera escrito no arquivo
	li 	$a2, 8			# Carrega numero de caracteres a serem escrito, nesse caso, 1(uma) word
	syscall				# Syscall -> Escrever no arquivo
	li	$v0, 15			# Diretiva-> Escrever no arquivo
	la 	$a1, mifspace		# Carrega endereco do conjunto de caracteres que serï¿½ escrito no arquivo
	li 	$a2, 3			# Carrega numero de caracteres a serem escrito, nesse caso, 3 caracteres
	syscall				# Syscall -> Escrever no arquivo
	la 	$s0, instructionpair	# Carrega o endereço de instruction pair
	lw	$s0, 0($s0)		# Carrega a instrução
	sw 	$s0, writingbuffer($zero)# Carrega no buffer de escrita o endereço
	la	$t3, writingbuffer	# Carrega o endereço do buffer de escrita em t3
	jal 	converthex		# Chama funcao de converter para hexadecimal o numero do contador
	li 	$t9, 0			# Zera o caso de hexadecimal
	li	$v0, 15			# Diretiva-> Escrever no arquivo
	move	$a1, $t3		# Carrega endereco do buffer que contem o que sera escrito no arquivo
	li 	$a2, 8			# Carrega numero de caracteres a serem escrito, nesse caso, 1(uma) word
	syscall				# Syscall -> Escrever no arquivo
	li	$v0, 15			# Diretiva-> Escrever no arquivo
	la 	$a1, semicolon		# Carrega endereco do buffer que contem o que sera escrito no arquivo
	li 	$a2, 2			# Carrega numero de caracteres a serem escrito, nesse caso, 1(uma) word
	syscall				# Syscall -> Escrever no arquivo
	lw	$s0, 0($sp)		# Recupera s0
	lw 	$ra, 4($sp)		# Recupera ra
	addi	$sp, $sp, 8		# Retorna o espaço na pilha
	jr 	$ra			# Retorna para caller
wrttxtheader:
	li   	$v0 , 15	
	move	$a0, $s6		# Diretiva-> Escrever no arquivo
	la 	$a1, headertxt		# Carrega o endereco da string que contem o header
	la	$a2, headertxtlen 	# Carrega o endereco que contem o valor do numero caracteres a serem escritos.		
	lw   	$a2, 0($a2)		# Carrega na forma inteira o valor do numero de caracteres a serem escritos. 	
	syscall				# Syscall -> Escrever no arquivo
	jr	$ra
wrttxtend:
	move  	$a0, $s6		# Codigo referente ao arquivo que sera escrito
	li	$v0, 15			# Diretiva-> Escrever no arquivo
	la 	$a1, end		# Carrega enderco que contem "\nEND"
	li	$a2, 5			# Numero de caracteres a ser escrito
	syscall				# Syscall -> Escrever no arquivo
	li   	$v0, 16       		# Fecha arquivo da funcao 
	move 	$a0, $s6      		# Codigo do arquivo a ser fechado
	syscall            		# Syscall -> Fechar arquivo
	j exit

#Funcao que	cria o arquivo de labels da parte .data
foundlabel:
	la 	 $t0, labelbuffer
	sw	 $zero, 0($t0)
	addi $t0, $t0, 4
	sw	 $zero, 0($t0)
	addi $t0, $t0, 4
	sw	 $zero, 0($t0)
	addi $t0, $t0, 4
	sw	 $zero, 0($t0)
	la 	$t0, labelbuffer
fndlblloop:
	addi $t3, $t3, -1		#Desloca o ponteiro para pegar o elemento anterior
	lbu  $t1,0($t3)			#Carrega um caractere do buffer
	beq	 $t1,'\n', seeklabels #Se for igual '\n', encontrei o primeiro caractere
	j	fndlblloop				#Continua a procurar o comeco da label
seeklabels:
	addi $t3, $t3, 1			#Desloca o ponteiro do buffer de leitura
	lbu $t1, 0($t3)				#Carrega o elemento
	beq	$t1, ':', endlabel		#Verifica se encontrou o fim da label
	sb $t1,0($t0)				#Se nao, armazena o byte lido
	addi $t0, $t0, 1			#Desloca o ponteiro do buffer de escrita
	j seeklabels
endlabel:
	addi $t0, $t0, 1			#Desloca o ponteiro 
	li 	 $t1, '\0'				#Carrega '\0'
	sb 	 $t1, 0($t0)			#Armazena '\0'
endlblloop:
	addi $t3, $t3, 1			#Desloca o ponteiro para pegar o proximo caractere
	lbu $t1, 0($t3)				#Carrega o caractere para comparar 
	beq	$t1, '.', fnddotaftlbl  #Encontrei um ponto depois de uma label, ela tem um endereco proprio
	beq	$t1, '\n', fndlfaftlbl  #Encontrei um '\n' depois de uma label, ela compartilha endereco
	j endlblloop					#Continua a procura um '.' ou um '\n'
fnddotaftlbl:
	la $t1, datalblcounter		#Carrega o endereço do contador para labels
	lw $t1, 0($t1)				#Carrega o elemento 
	li $t0, 16					#Atribui $t0 <- 16
	sw $t1, labelbuffer($t0)	#Guarda no local para que possa ser escrito
	addi $t1, $t1, 4			#Adiciona 4 ao contador
	sw $t1, datalblcounter($zero)	#Volta o novo valor do contador a memoria
	move $s7, $t3				#Salva $t3
	jal wrtlabelfile			#Pula para funcao que escrevera no arquivo
	move $t3, $s7				#Retorna $t3 para seu valor original
	j readbytesloop				#PODE DAR ERRO AQUI, PRESTAR ATENCAO
fndlfaftlbl:
	la $t1, datalblcounter		#Carrega o endereço do contador para labels
	lw $t1, 0($t1)				#Carrega o elemento 
	li $t0, 16					#Atribui $t0 <- 16
	sw $t1, labelbuffer($t0)	#Guarda no local para que possa ser escrito
	move $s7, $t3				#Salva $t3
	jal wrtlabelfile			#Pula para funcao que escrevera no arquivo
	move $t3, $s7				#Retorna $t3 para seu valor original
	j readbytesloop				#Continua a ler os caracteres


#Funcaoo que faz o processamento da parte .data
wrtdatafile:
	li 	$v0, 13			# Diretiva ->Abrir arquivo
	la 	$a0, file    		# Abrir  arquivo temporario
	li	$a1, 0       	 	# Modo Leitura
	li 	$a2, 0        		# Ignora o modo
	syscall 			# Abrir arquivo
	move 	$s0, $v0		# Salva identificador do arquivo
	li   	$v0, 13       		# Deretiva -> Abrir arquivo
  	la   	$a0, datamif   		# Nome do arquivo de saida
  	li   	$a1, 9        		# Modo escrita
  	li   	$a2, 0        		# Ignorar modo
  	syscall           		# Syscall -> Abrir arquivo 
  	move 	$s6, $v0		# Salva o identificador do arquivo
  	j wrtheader				#Escreve o header do arquivo
readdataloop:
	li   	$v0, 14      	#Diretiva -> Ler arquivo
	move 	$a0, $s0      	#Arquivo que sera lido
	la   	$a1, databuffer #Buffer para os dados que serao lidos
	li  	$a2, 4096      	#Numero de bytes a serem lidos -> 32
	syscall 				#Ler arquivo
	slti 	$t0, $v0, 1		#Verifica se houve erro
	beq 	$t0, 1, exitfail	#Se houve erro, para execucao
	la 	$t3, databuffer		#Carrega endereco do buffer
readbytesloop:				#Loop para continuar lendo bytes do arquivo
	lbu 	$t1,0($t3)			#Carrega um caractere do buffer	
	beq		$t1,':', foundlabel	#Verifica se encontrou uma label
	beq 	$t1,'.', founddot	#Ve se encontrou um '.'
	la  	$t0, databuffer		#Carrega o endereco do buffer
	add 	$t0, $t0, 512		#Move o ponteiro para o ultimo elemento
	addi 	$t3, $t3, 1
	bne 	$t3, $t0, readbytesloop #Ve se ja percorreu todos elementos, se nao, le mais
	j readdataloop			#Se ja percourreu le mais do arquivo
founddot:					#Caso tenha encontrado um ponto
	lw 	$t0, data_label($zero)	#Carrega a label "data"
	addi $t3,$t3, 4				#Move 4 bytes para frente
	lbu $t4, 0($t3)				#Carrega em t4 o valor do byte final
	sll $t4, $t4, 24			#Desloca o byte para que fique na forma 0xab000000
	addi $t3,$t3, -1			#Volta para o byte anterior
	lbu $t5, 0($t3)				#Carrega em t5 o valor do byte atual
	sll $t5, $t5, 16			#Desloca o byte para que fique na forma 0x00cd0000
	or $t4, $t4, $t5			#And para para armazenar em t4 ->0xabcd0000
	li 	$t5, 0					#Zera t5
	addi $t3,$t3, -1			#Volta para o byte anterior
	lbu $t5, 0($t3)				#Carrega em t5 o valor do byte atual
	sll $t5, $t5, 8				#Desloca o byte para que fique na forma 0x0000ef00
	addi $t3,$t3, -1			#Volta para o byte anterior
	or $t4, $t4, $t5			#And para em t4 -> 0xabcdef00
	lbu $t5, 0($t3)				#Carrega em t5 o valor do byte atual para que fique da forma 0x0000efgh
	or $t4, $t4, $t5			#And para em t4 -> 0xabcdefgh
	beq	$t4, $t0, readbytesloop		#Compara se a word encontrada com "data", caso seja igual, escreve o header
	lw	$t0, dotword($zero)		#Carrega a label "word"
	beq	$t4, $t0, wrtdatamem		#Compara se a word encontrada com "word", caso seja igual, escrever no arquivo
	lw	$t0, text_label($zero)		#Carrega a label "text"
	beq	$t4, $t0, wrtend		#Caso encontre a label ".text", a escrita deve ser finalizada
	j  readbytesloop			#Continua a ler os bytes
wrtdatamem:
	addi $t3, $t3, 5			#Pequeno fix, devido a outras funcoes, volta o ponteiro pro lugar correto
wdmloop:			
	lbu $t1,0($t3)				#Carrega o valor da memoria
	beq $t1, '\0', next2		#Verifica se e '\0', se for continuar a ler
	beq $t1, ' ', next2			#Verifica se e ' ', se for continuar a ler
	beq $t1, ',', next2			#Verifica se e ',', se for continuar a ler
	beq $t1, '\n', readbytesloop #Verifica se e '\n', se for acabar leitura
	beq $t1, '\r', readbytesloop #Verifica se e '\r', se for acabar leitura
	beq $t1, '0', checkifhex     #Serve para compor funcao de verificacao de hexadecimais
contwdmloop: 
	jal convertint				#Converte o numero para inteiro
	add $t3, $t3, 1				#Desloca o ponteiro	
	j wrtdata					#Escreve no arquivo
	la  $t0, databuffer			#Carrega o endereco do buffer
	add $t0, $t0, 512			#Carrega o endereco do final do buffer
	bne $t3, $t0, wdmloop		#Caso o endereco atual nao seja o final, continua a ler 
	j readdataloop				#Se for o final, ler mais do arquivo
checkifhex:
	addi $t3, $t3, 1			#Desloca o ponteiro para pegar o segundo caractere
	lbu $t1,0($t3)				#Carrega o segundo caractere
	beq $t1, 'x', alreadyhex	#Verifica se o caractere e igual a 'x', se for pula para tratamento
	addi $t3, $t3, -1			#Se nao for, retorna o ponteiro para poder ler o numero
	j contwdmloop				#Continua para poder fazer tratamento de inteiros
next2:
	addi $t3, $t3, 1			#Desloca o ponteiro
	j wdmloop					#Continua a ler
alreadyhex:			
	la $t9, writingbuffer		#Carrega endereco
	la $s7, writingbuffer		#Carrega endereco
	sw $zero, 0($t9)			#Zera os elementos no buffer
	sw $zero, 4($t9)			#Zera os elementos no buffer
ahloop:
	li $t9, 16					#Codigo que deve indicar que o valor lido e hexadecimal
	addi $t3, $t3, 1			#Desloca o ponteiro
	lbu $t1,0($t3)				#Carrega o elemento
	beq $t1, '\0', wrtdata		#Verifica se o elemento e '\0', indicando fim do valor
	beq $t1, ' ', wrtdata		#Verifica se o elemento e ' ', indicando fim do valor
	beq $t1, ',', wrtdata		#Verifica se o elemento e ',', indicando fim do valor
	beq $t1, '\n', wrtdata		#Verifica se o elemento e '\n', indicando fim do valor
	beq $t1, '\r', wrtdata		#Verifica se o elemento e '\r', indicando fim do valor
	sb  $t1, 0($s7)				#Coloca o elemento lido na memoria
	addi $s7, $s7, 1 			#Desloca o ponteiro 
	j ahloop					#Continua a ler o hexadecimal
exitfail:
	li $v0, 10
	syscall

#Funcao responsavel pela escrita dos dados no arquivo, e a funcao principal para essa tarefa, nao recebe argumento, porem,
#tudo que deve ser escrito deve estar contido na memoria em 'writingbuffer'
wrtdata:
	addi 	$sp, $sp, -16		# Abre espaco na pilha
	sw 	$ra, 0($sp)				#Armazena o registrador de retorno
	sw 	$t3, 8($sp)				#Armazena o primeiro elemento de writingbuffer
ncall:
	la  	$t3, writingbuffer	# Carrega o endereï¿½o do buffer de escrita
	lw  	$t1, 0($t3)		# Carrega o codigo da funcao que sera escrito
	sw 	$t1, 4($sp)		# Coloca o codigo da funcao na pilha para salva-lo
	lw 	$t1, 4($t3)		#Carrega o segundo elemento de writingbuffer
	sw	$t1, 12($sp)	#Armazena o segundo elemento de writingbuffer
 	lw 	$t1, counter($zero)	# Recebe o valor do contador
	sw 	$t1, 0($t3)		# Coloca o valor do contador na memoria para ser escrito
contprocess:
	jal 	converthex		# Chama funcao de converter para hexadecimal o numero do contador
	li	$v0, 15			# Diretiva-> Escrever no arquivo
	move	$a0, $s6
	la 	$a1, writingbuffer	 # Carrega endereco do buffer que contem o que sera escrito no arquivo
	li 	$a2, 8			# Carrega numero de caracteres a serem escrito, nesse caso, 1(uma) word
	syscall				# Syscall -> Escrever no arquivo
	lw	$t1, counter($zero)
	addi 	$t1, $t1, 4		# Incrementa o contador em 4
	sw 	$t1, counter($zero)		# Retorna o valor incrementado do contador da memoria
	li	$v0, 15			# Diretiva-> Escrever no arquivo
	la 	$a1, mifspace		# Carrega endereco do conjunto de caracteres que serï¿½ escrito no arquivo
	li 	$a2, 3			# Carrega numero de caracteres a serem escrito, nesse caso, 3 caracteres
	syscall				# Syscall -> Escrever no arquivo
	lw	$t1,4($sp)		#Carrega o primeiro elemento que estava em writingbuffer
	sw	$t1, writingbuffer($zero)		#Retorna o elemento de writingbuffer ao seu lugar
	sw 	$t1, datalblcounter($zero) #Altera contador para labels
	lw	$t1,12($sp)		#Carrega o segundo elemento que estava em writingbuffer
	sw 	$t1,4($t3) 		#Retorna o elemento de writingbuffer ao seu lugar
	beq	$t9, 16, readashex #Ve se tem que tratar como hexadecimal
	jal	converthex		# Chama a funcao de converter para hexadecimal o codigo da funcao
readashex:	
	li 	$t9, 0			#Zera o caso de hexadecimal
	li	$v0, 15			# Diretiva-> Escrever no arquivo
	move	$a1, $t3		# Carrega endereco do buffer que contem o que sera escrito no arquivo
	li 	$a2, 8			# Carrega numero de caracteres a serem escrito, nesse caso, 1(uma) word
	syscall				# Syscall -> Escrever no arquivo

	li	$v0, 15			# Diretiva-> Escrever no arquivo
	la 	$a1, semicolon		# Carrega endereco do buffer que contem o que sera escrito no arquivo
	li 	$a2, 2			# Carrega numero de caracteres a serem escrito, nesse caso, 1(uma) word
	syscall				# Syscall -> Escrever no arquivo
	lw 	$ra, 0($sp)		#Retorna ao valor original
	lw	$t3, 8($sp)		#Retorna ao valor original

	j 	wdmloop			#Continua a escrever

#Funcao responsavel por escrever o 'header' do arquivo, chama-la apenas em 'wrtdata' ou de forma avulsa caso 
#seja a primeira coisa a escrever no arquivo
wrtheader:
	move 	$a0, $s6		# Codigo referente ao arquivo que sera escrito
	li   	$v0 , 15		# Diretiva-> Escrever no arquivo
	la 	$a1, header		# Carrega o endereco da string que contem o header
	la	$a2, headerlen 		# Carrega o endereco que contem o valor do numero caracteres a serem escritos.		
	lw   	$a2, 0($a2)		# Carrega na forma inteira o valor do numero de caracteres a serem escritos. 	
	syscall				# Syscall -> Escrever no arquivo
	j 	readdataloop
#
#Escreve apenas a parte "END;\n" do arquivo, chama-la apenas ao finalizar a escrita de todos os dados do arquivo"
wrtend:
	move  	$a0, $s6		# Codigo referente ao arquivo que sera escrito
	li	$v0, 15			# Diretiva-> Escrever no arquivo
	la 	$a1, end		# Carrega enderco que contem "\nEND"
	li	$a2, 5			# Numero de caracteres a ser escrito
	syscall				# Syscall -> Escrever no arquivo
	li   	$v0, 16       		# Fecha arquivo da funcao wrtdata
	move 	$a0, $s0      		# Codigo do arquivo a ser fechado
	syscall            		# Syscall -> Fechar arquivo
	move 	$a0, $s6
	j  Abrir

#Funcao responsavel por converter um numero em ascii para um numero hexadecimal
convertaschex:	
	li  $s7, -1			
	li  $t2, 0			#Carrega $t2  <- 0
countloophex:
	lbu $t1, 0($t3)		#Carrega o primeiro caractere
	beq	$t1, ',', castandsumhex	#Verifica se e ',', se for o numero terminou
	beq $t1, '\n', castandsumhex	#Verifica se e '\n', se for o numero terminou
	beq $t1, '\r', castandsumhex	#Verifica se e '\r', se for o numero terminou
	beq	$t1, ' ',  castandsumhex	#Verifica se e ' ', se for o numero terminou
	beq	$t1, '\0',  castandsumhex  #Verifica se e '\0', se for o numero terminou
	addi $t3, $t3, 1			#Desloca 1 byte para continuar lendo o numero
	addi $t2, $t2, 1			#Soma 1 ao contador
	addi $s7, $s7, 1
	j countloophex					#Continua contando
castandsumhex:
	sub $t3, $t3, $t2			#Volta ao inicio do numero
	li $t0, 1					#Carrega $t0 <- 1
power16:
	addi $t2, $t2, -1			#Remove 1 do contador
	beq $t2, 0, casthex
	mul $t0, $t0, 16			#Multiplica $t0 por 16 para formar as potencias
	bne $t2, 1, power16			#Ate que a potencia real seja obtida, continua
	li  $t2, 0					#Carrega $t2 <- 0 para poder armazenar o valor da soma das potencias
casthex:
	lbu $t1, 0($t3)				#Carrega o primeiro byte
	blt $t1, 0x61, contcst
	addi $t1, $t1, -39			#Aplica cast char -> int
contcst:
	addi $t1, $t1, -48			#Aplica cast char -> int
	mul $t1, $t1, $t0			#Multiplica o valor int por sua potencia
	div $t0, $t0, 16			#Divide por 10 para encontrar a proxima potencia
	add $t2, $t2, $t1			#Soma o valor obtido ao valor anterior
	addi $t3, $t3, 1			#Desloca o ponteiro para pegar o proximo valor
	beq $t0, 0, contcasthex
	bne	$t0, 1, casthex			#Se nao tiver dado cast em n-1 caracteres, continua dando cast
contcasthex:
	beq $s7, 0, fixhex
	lbu $t1, 0($t3)				#Carrega o primeiro byte
	blt $t1, 0x61, contcst2
	addi $t1, $t1, -39			#Aplica cast char -> int
contcst2:
	addi $t1, $t1, -48			#Aplica cast char -> int
	add $t2, $t2, $t1			#Soma o valor obtido ao valor anterior
	sw $t2, writingbuffer($zero)#Coloca o valor na memoria para ser utilizado posteriormente
	jr $ra
fixhex:
	addi $t3, $t3, -1
	sw $t2, writingbuffer($zero)#Coloca o valor na memoria para ser utilizado posteriormente 
	jr $ra

#Funcao responsavel por converter um numero em ascii para um numero inteiro
convertint:
	addi	$sp, $sp, -4
	sw	$s5, 0($sp)	
	li  $s5, -1			
	li  $t2, 0			#Carrega $t2  <- 0
countloop:
	lbu $t1, 0($t3)		#Carrega o primeiro caractere
	beq	$t1, ',', castandsum	#Verifica se e ',', se for o numero terminou
	beq $t1, '\n', castandsum	#Verifica se e '\n', se for o numero terminou
	beq $t1, '\r', castandsum	#Verifica se e '\r', se for o numero terminou
	beq	$t1, ' ',  castandsum	#Verifica se e ' ', se for o numero terminou
	beq	$t1, '\0',  castandsum  #Verifica se e '\0', se for o numero terminou
	addi $t3, $t3, 1			#Desloca 1 byte para continuar lendo o numero
	addi $t2, $t2, 1			#Soma 1 ao contador
	addi $s5, $s5, 1
	j countloop					#Continua contando
castandsum:
	sub $t3, $t3, $t2			#Volta ao inicio do numero
	li $t0, 1					#Carrega $t0 <- 1
power10:
	addi $t2, $t2, -1			#Remove 1 do contador
	beq $t2, 0, cast
	mul $t0, $t0, 10			#Multiplica $t0 por 10 para formar as potencias
	bne $t2, 1, power10			#Ate que a potencia real seja obtida, continua
	li  $t2, 0					#Carrega $t2 <- 0 para poder armazenar o valor da soma das potencias
cast:
	lbu $t1, 0($t3)				#Carrega o primeiro byte
	addi $t1, $t1, -48			#Aplica cast char -> int
	mul $t1, $t1, $t0			#Multiplica o valor int por sua potencia
	div $t0, $t0, 10			#Divide por 10 para encontrar a proxima potencia
	add $t2, $t2, $t1			#Soma o valor obtido ao valor anterior
	addi $t3, $t3, 1			#Desloca o ponteiro para pegar o proximo valor
	beq $t0, 0, contcast
	bne	$t0, 1, cast			#Se nao tiver dado cast em n-1 caracteres, continua dando cast
contcast:
	beq $s5, 0, fix
	lbu $t1, 0($t3)				#Carrega o primeiro byte
	addi $t1, $t1, -48			#Aplica cast char -> int
	add $t2, $t2, $t1			#Soma o valor obtido ao valor anterior
	sw $t2, writingbuffer($zero)#Coloca o valor na memoria para ser utilizado posteriormente
	lw	$s5, 0($sp)
	addi	$sp, $sp, 4
	jr $ra 
fix:
	addi $t3, $t3, -1
	sw $t2, writingbuffer($zero)#Coloca o valor na memoria para ser utilizado posteriormente
	lw	$s5, 0($sp)
	addi	$sp, $sp, 4
	jr $ra 

#Funcao responsavel pela conversao de binario para hexadecimal, toma como argumento $t3 que deve conter o endereco inicial de 
#'writingbuffer', ela sempre deve receber 1(word) caso contrario devolvera lixo. 
converthex:
	lw 	$t2, 0($t3)		# Carrega primeiro dado do buffer
#Comparacao primeiro nible
st_nible:
	andi 	$t1, $t2, 0x0000000f	# Mascara para obter primeiro nible
	slti 	$t0, $t1, 10		# Verifica se o primeiro nible e menor que 10
	bne 	$t0, 1, cont_st		# Caso nao seja pula para funcao que transformara em ascii a-f
	addi 	$t1, $t1, 0x30		# Caso seja menor que 10 transforma o numero em ascii 0-9
	j	store_st		# Pulo para evitar continuacao
cont_st:
	addi 	$t1, $t1, 87		# Transforma para ascii a-f
store_st:	
	sb 	$t1, 7($t3)		# Store nible (8) -> memoria	
#Comparacao segundo nible
nd_nible:
	andi 	$t1, $t2, 0x000000f0	# Mascara para obter segundo nible
	srl 	$t1, $t1, 4		# Desloca segundo nible para obter valor real
	slti 	$t0, $t1, 10		# Verifica se o segundo nible e menor que 10
	bne 	$t0, 1, cont_nd		# Caso nao seja pula para funcao que transformara em ascii a-f
	addi 	$t1, $t1, 0x30		# Caso seja menor que 10 transforma o numero em ascii 0-9
	j 	store_nd		# Pulo para evitar continuacao
cont_nd:
	addi 	$t1, $t1, 87		# Transforma para ascii a-f
store_nd:
	sb 	$t1,	6($t3)		# Store nible (7) -> memoria	
#Comparacao terceiro nible
rd_nible:
	andi 	$t1, $t2, 0x00000f00	# Mascara para obter terceiro nible
	srl 	$t1, $t1, 8		# Desloca terceiro nible para obter valor real
	slti 	$t0, $t1, 10		# Verifica se o terceiro nible e menor que 10
	bne 	$t0, 1, cont_rd		# Caso nao seja pula para funcao que transformara em ascii a-f
	addi 	$t1, $t1, 0x30		# Caso seja menor que 10 transforma o numero em ascii 0-9
	j 	store_rd		# Pulo para evitar continuacao
cont_rd:
	addi 	$t1, $t1, 87		# Transforma para ascii a-f
store_rd:
	sb 	$t1, 5($t3)		# Store nible (6) -> memoria	

#Comparacao quarto nible
frth_nible:
	andi 	$t1, $t2, 0x0000f000	# Mascara para obter quarto nible
	srl 	$t1, $t1, 12		# Desloca quarto nible para obter valor real
	slti 	$t0, $t1, 10		# Verifica se o quarto nible e menor que 10
	bne 	$t0, 1, cont_frth	# Caso nao seja pula para funcao que transformara em ascii a-f
	addi 	$t1, $t1, 0x30		# Caso seja menor que 10 transforma o numero em ascii 0-9
	j 	store_frth		# Pulo para evitar continuacao
cont_frth:
	addi 	$t1, $t1, 87		# Transforma para ascii a-f
store_frth:
	sb 	$t1, 4($t3)		# Store nible (5) -> memoria	
#Comparacao quinto nible
fifth_nible:
	andi 	$t1, $t2, 0x000f0000	# Mascara para obter quinto nible
	srl 	$t1, $t1, 16		# Desloca quinto nible para obter valor real
	slti 	$t0, $t1, 10		# Verifica se o quinto nible e menor que 10
	bne 	$t0, 1, cont_fifth	# Caso nao seja pula para funcao que transformara em ascii a-f
	addi 	$t1, $t1, 0x30		# Caso seja menor que 10 transforma o numero em ascii 0-9
	j 	store_fifth		# Pulo para evitar continuacao
cont_fifth:
	addi 	$t1, $t1, 87		# Transforma para ascii a-f
store_fifth:
	sb 	$t1, 3($t3)		# Store nible (4) -> memoria	
#Comparacao sexto nible
sixth_nible:
	andi 	$t1, $t2, 0x00f00000	# Mascara para obter sexto nible
	srl 	$t1, $t1, 20		# Desloca sexto nible para obter valor real
	slti 	$t0, $t1, 10		# Verifica se o sexto nible e menor que 10
	bne 	$t0, 1, cont_sixth	# Caso nao seja pula para funcao que transformara em ascii a-f
	addi 	$t1, $t1, 0x30		# Caso seja menor que 10 transforma o numero em ascii 0-9
	j 	store_sixth		# Pulo para evitar continuacao
cont_sixth:		
	addi	$t1, $t1, 87		# Transforma para ascii a-f
store_sixth:
	sb 	$t1, 2($t3)		# Store nible (3) -> memoria					
#Comparacao setimo nible
sevth_nible:	
	andi 	$t1, $t2, 0x0f000000	# Mascara para obter setimo nible
	srl 	$t1, $t1, 24		# Desloca setimo nible para obter valor real
	slti 	$t0, $t1, 10		# Verifica se o setimo nible e menor que 10
	bne 	$t0, 1, cont_sevth	# Caso nao seja pula para funcao que transformara em ascii a-f
	addi 	$t1, $t1, 0x30		# Caso seja menor que 10 transforma o numero em ascii 0-9
	j 	store_sevth		# Pulo para evitar continuacao
cont_sevth:
	addi 	$t1, $t1, 87		# Transforma para ascii a-f
store_sevth:	
	sb 	$t1, 1($t3)		# Store nible (2) -> memoria	
#Comparacao oitavo nible
final_nible:
	andi 	$t1, $t2, 0xf0000000	# Mascara para obter oitavo nible
	srl 	$t1, $t1, 28		# Desloca oitavo nible para obter valor real
	slti 	$t0, $t1, 10		# Verifica se o oitavo nible e menor que 10
	bne 	$t0, 1, cont_final	# Caso nao seja pula para funcao que transformara em ascii a-f
	addi 	$t1, $t1, 0x30		# Caso seja menor que 10 transforma o numero em ascii 0-9
	j 	store_final
cont_final:
	addi 	$t1, $t1, 87		# Transforma para ascii a-f	
store_final:
	sb 	$t1, 0($t3)		# Store nible (1) -> memoria			
	jr 	$ra			# Retorna para a funcao que a chamou

exit_ok:	
	jal 	wrttxtend		# Finaliza o arquivo
	li   	$v0, 16       		# Fecha arquivo da funcao wrtdata
	move 	$a0, $s0      		# Codigo do arquivo a ser fechado
	syscall            		# Syscall -> Fechar arquivo

exit:	
	sw $v0, dbg
	li   	$v0, 16       		# Fecha arquivo da funcao wrtdata
	move 	$a0, $s6      		# Codigo do arquivo a ser fechado
	syscall            		# Syscall -> Fechar arquivo
